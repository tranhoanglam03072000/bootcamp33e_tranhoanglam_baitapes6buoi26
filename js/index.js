//Bài 1--------------------------------------------------------
let colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let listButton = ``;
colorList.forEach((item) => {
  let button = `<button class="color-button ${item}"></button>`;
  listButton += button;
});
domID("colorContainer").innerHTML = listButton;
let buttonList = domID("colorContainer");
let allButton = buttonList.querySelectorAll("button");
allButton[0].classList.add("active");
let house = domID("house");
for (let i = 0; i < colorList.length; i++) {
  allButton[i].onclick = () => {
    house.className = `house ${colorList[i]} `;
    let activeButton = buttonList.querySelector(".active");
    activeButton.classList.remove("active");
    allButton[i].classList.add("active");
  };
}

// Bài 2-----------------------------------------------
let tinhDtb = (...diemMon) => {
  let sum = 0;
  diemMon.forEach((item) => {
    sum += item;
  });
  return sum / diemMon.length;
};

let btnKhoi1 = () => {
  let diemToan = domID("inpToan").value * 1;
  let diemLy = domID("inpLy").value * 1;
  let diemHoa = domID("inpHoa").value * 1;
  domID("tbKhoi1").innerHTML = tinhDtb(diemToan, diemLy, diemHoa).toFixed(2);
};
let btnKhoi2 = () => {
  let diemVan = domID("inpVan").value * 1;
  let diemSu = domID("inpSu").value * 1;
  let diemDia = domID("inpDia").value * 1;
  let diemAnh = domID("inpEnglish").value * 1;
  domID("tbKhoi2").innerHTML = tinhDtb(
    diemVan,
    diemSu,
    diemDia,
    diemAnh
  ).toFixed(2);
};
// Bài 3---------------------------------------------------
let textHover3 = domID("textHover3").innerText;
let arrTextHover3 = [...textHover3];
let ketQua3 = ``;
arrTextHover3.forEach((item) => {
  let oneTextHover = `<span>${item}</span>`;
  ketQua3 += oneTextHover;
});
domID("textHover3").innerHTML = ketQua3;
